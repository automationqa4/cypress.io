/// <reference types="cypress" />

import Navbar from "../pageObject/navbar";
import Register, { password } from "../pageObject/registrationForm";

describe('Registeration Form', () => {

    beforeEach(() => {
        cy.visit('');
    })

    const register = new Register();
    const navbar = new Navbar();

    it('1. Register new user', () => {
        navbar.getAccountButton().click();
        register.waitForAccountUrlToBeLoaded();
        register.enterRegistrationEmailInputField();
        register.enterRegisterationPasswordInputField();
        register.getRegisterButton().click();
    });

    it('2. Registration form, email and password validation', () => {
        navbar.getAccountButton().click();
        register.waitForAccountUrlToBeLoaded();
        register.getRegisterButton().click();
        register.getValidationMessageNoEmailAndPassword();
        register.enterRegistrationEmailInputField();
        register.getRegisterButton().click();
        register.getValidationMessageNoPassword();
        register.getRegistrationEmailInputField().clear();
        register.enterRegisterationPasswordInputField();
        register.getRegisterButton().click();
        register.getValidationMessageNoEmail();
    });

    it('3. Registration form, password strengh check', () => {
        navbar.getAccountButton().click();
        register.waitForAccountUrlToBeLoaded();
        register.enterRegistrationEmailInputField();
        register.getRegisterationPasswordInputField()
            .type(password.VeryWeakPassword);
        register.checkPasswordStrenghVeryWeak();
        register.getRegisterationPasswordInputField().clear();
        register.getRegisterationPasswordInputField()
            .type(password.WeakPassword);
        register.checkPasswordStrenghWeak();
        register.getRegisterationPasswordInputField().clear();
        register.getRegisterationPasswordInputField()
            .type(password.MediumPassword);
        register.checkPasswordStrenghMedium();
        register.getRegisterationPasswordInputField().clear();
        register.getRegisterationPasswordInputField()
            .type(password.StrongPassword);
        register.checkPasswordStrenghStrond();
    });
})