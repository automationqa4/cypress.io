/// <reference types="cypress" />

import Account from "../pageObject/account";
import Login, { user } from "../pageObject/login";
import Navbar from "../pageObject/navbar";
import SharedTests from "../shared/sharedtests";

describe("Login Test", () => {
  const login = new Login();
  const navbar = new Navbar();
  const account = new Account();

  beforeEach(() => {
    cy.visit('');
  });
  it('1. Login with correct credentials with user name', () => {
    navbar.getAccountButton().click();
    login.getLoginPage();
    login.getUserNameInput().type(user.userName);
    login.getPasswordInput().type(user.password);
    login.getLoginButton().click();
    login.getLogoutButton();
  });
  it('2. Login with correct credentials with email', () => {
    SharedTests.UserLogin();
  });
  it('3. Login with wrong credentials"', () => {
    navbar.getAccountButton().click();
    login.getLoginPage();
    login.getUserNameInput().type(user.incorrectEmail);
    login.getPasswordInput().type(user.password);
    login.getLoginButton().click();
    login.getValidationIncorrectEmail();
    login.getUserNameInput().clear().type(user.incorrectUserName);
    login.getPasswordInput().type(user.password);
    login.getLoginButton().click();
    login.getValidationIncorrectUserName();
    login.getUserNameInput().clear().type(user.email);
    login.getPasswordInput().type(user.incorrectPassword);
    login.getLoginButton().click();
    login.getValidationIncorrectPassword();
  });
  it('4. Login without username data', () => {
    navbar.getAccountButton().click();
    login.getLoginPage();
    login.getPasswordInput().type(user.password);
    login.getLoginButton().click();
    login.getValidationEmptyUsernNameInput();
  });
  it('5. Login without password', () => {
    navbar.getAccountButton().click();
    login.getLoginPage();
    login.getUserNameInput().type(user.email);
    login.getLoginButton().click();
    login.getValidationEmptyPasswordInput();
  });
  it('6. Login with empty username and password', () => {
    navbar.getAccountButton().click();
    login.getLoginPage();
    login.getLoginButton().click();
    login.getValidationEmptyUsernNameInput();
  });
  // it('7. Logout', () => {
  //   SharedTests.UserLogin()
  //   account.getLogOutButtonFromMenu()
  //   login.getLogoutConfirmPageUrl()
  //   login.getLogoutConfirmButton().click()
  // });
})
