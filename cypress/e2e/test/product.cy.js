/// <reference types="cypress" />

import Cart from "../pageObject/cart";
import Navbar from "../pageObject/navbar";
import Product, { item } from "../pageObject/product";
import SharedTests from "../shared/sharedtests";

describe('Adding product to bascet and finalizing purchaise', () => {
    
    beforeEach (() => {
        cy.visit('')
        SharedTests.RegisterUser();
    })

    const navbar = new Navbar();
    const product = new Product();
    const cart = new Cart();

    it('1. Product searching', () => {
        navbar.getSearchInputField().type(item.firstItem);
        navbar.getSearchButton().click();
        product.getFoundProduct(item.firstItem).click();
        product.getProductPageUrl(item.firstItem);
    });

    it('2. Adding products to cart and checking cart content', () => {
        navbar.getSearchInputField().type(item.firstItem);
        navbar.getSearchButton().click();
        product.getFoundProduct(item.firstItem).click();
        product.getProductPageUrl(item.firstItem);
        product.getQuantityInputField().clear();
        product.getQuantityInputField().type(2);
        product.getAddToCartButton().click();
        navbar.getSearchInputField().type(item.secondItem);
        navbar.getSearchButton().click();
        product.getFoundProduct(item.secondItem).click();
        product.getProductPageUrl(item.secondItem);
        product.getQuantityInputField().clear();
        product.getQuantityInputField().type(1)
        product.getAddToCartButton().click();
        navbar.getSearchInputField().type(item.thirdItem);
        navbar.getSearchButton().click();
        product.getFoundProduct(item.thirdItem).click();
        product.getProductPageUrl(item.thirdItem);
        product.getQuantityInputField().clear();
        product.getQuantityInputField().type(3)
        product.getAddToCartButton().click();
        navbar.getCartButton().click();
        cart.getCartPageUrl();
        cart.getProductQuantity(item.firstItem, 2);
        cart.getProductQuantity(item.secondItem, 1);
        cart.getProductQuantity(item.thirdItem, 3);
        cart.checkTotalPriceEqualsUnitPriceMultipliedByQuantity(item.firstItem, 2);
        cart.checkTotalPriceEqualsUnitPriceMultipliedByQuantity(item.secondItem, 1);
        cart.checkTotalPriceEqualsUnitPriceMultipliedByQuantity(item.thirdItem, 3);
    });
});