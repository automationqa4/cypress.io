import Login from "../pageObject/login";
import Navbar from "../pageObject/navbar";
import Register from "../pageObject/registrationForm";

const login = new Login()
const navbar = new Navbar()
const register = new Register()

export const generatedEmail = {
    UserName: `guild.qa.testing+${Date.now()}`,
    Domain: '@gmail.com'
}
export const password= `GuildQATeam1!+${Date.now()}`;

export default class SharedTests {
    static UserLogin() {

        navbar.getAccountButton().click();
        login.getLoginPage();
        login.getUserNameInput().type(generatedEmail.UserName+generatedEmail.Domain);
        login.getPasswordInput().type(password);
        login.getLoginButton().click();
        login.getLogoutButton();
    }

    static RegisterUser() {

        navbar.getAccountButton().click();
        register.waitForAccountUrlToBeLoaded();
        register.enterRegistrationEmailInputField();
        register.enterRegisterationPasswordInputField();
        register.getRegisterButton().click();
    }
}