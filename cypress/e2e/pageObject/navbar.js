export const accountButton = ' Account ';
export const searchInputField = '.search-field-top-bar';
export const searchButton = '.search-top-bar-submit';
export const cartButton = '.top-cart';

export default class Navbar {
    getAccountButton() {
        return cy.contains(accountButton);
    }

    getSearchInputField() {
        return cy.get(searchInputField);
    }

    getSearchButton() {
        return cy.get(searchButton);
    }

    getCartButton() {
        return cy.get(cartButton);
    }
}