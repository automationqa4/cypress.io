export const userNameInput = "#username";
export const passwordInput = "#password";
export const validationMessage = {
    emptyUserName: " Username is required.",
    emptyPassword: " The password field is empty.",
    incorrectUserName: " is not registered on this site. If you are unsure of your username, try your email address instead.",
    incorrectEmail: " A user could not be found with this email address.",
    incorrectPassword: " The password you entered for the username "
};
export const user = {
    userName: "tester222",
    email: "tester222@gmail.com",
    password: "Testertester1!",
    incorrectUserName: "yesthythyj",
    incorrectEmail: "test111@gmail.com",
    incorrectPassword: "Tester1!"
  };
export const loginPageUrl = "/my-account";
export const loginButton = 'input[name="login"]';
export const logoutButton = "Log out";
export const logoutConfirmPageUrl = "/my-account/customer-logout";
export const logoutConfirmButton = "Confirm and log out";
export const homePageUrl = "https://skleptest.pl/";

export default class Login {
    getUserNameInput(){
        return cy.get(userNameInput);
    }
    getPasswordInput(){
        return cy.get(passwordInput);
    }
    getValidationEmptyUsernNameInput() {
        return cy.contains(validationMessage.emptyUserName);
      }
    getValidationEmptyPasswordInput() {
        return cy.contains(validationMessage.emptyPassword);
      }
    getValidationIncorrectUserName() {
        return cy.contains(validationMessage.incorrectUserName);
      }
    getValidationIncorrectEmail() {
        return cy.contains(validationMessage.incorrectEmail);
      }
    getValidationIncorrectPassword() {
        return cy.contains(validationMessage.incorrectPassword);
      }
    getLoginPage(){
        return cy.url().should('include', loginPageUrl);
    }
    getLoginButton(){
        return cy.get(loginButton);
    }
    getLogoutButton(){
        return cy.contains(logoutButton);
    }
    getLogoutConfirmPageUrl(){
        return cy.url().should('include', logoutConfirmPageUrl);
    }
    getLogoutConfirmButton(){
        return cy.contains(logoutConfirmButton);
    }
    getHomePageUrl(){
        return cy.url().should('include', homePageUrl);
    }

}