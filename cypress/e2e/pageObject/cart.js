import Product from "./product";

export const productQuantity = ".input-text qty text"
export const cartUrl = "https://skleptest.pl/cart/";

export default class Cart {
    constructor() {
        this.product = new Product();
    }

    getProductQuantity(selectedItem, quantity) {
        this.product.getFoundProduct(selectedItem)
            //cy.get('.product-name a:contains("Manago Shirt")')
            .parent('td.product-name')
            .siblings('td.product-quantity')
            .find('.input-text.qty.text')
            .should('have.value', quantity.toString());
    }

    checkTotalPriceEqualsUnitPriceMultipliedByQuantity(selectedItem, quantity) {
        //Find Unit price
        this.product.getFoundProduct(selectedItem)
            .parent('td.product-name')
            .siblings('td.product-price')
            .find('.woocommerce-Price-amount.amount')
            .invoke('text')
            .then((unitPriceText) => {
                //Text conversion to number
                const unitPriceWithoutCurrency = unitPriceText.replace(/[^0-9]/g, '');
                const unitPrice = parseInt(unitPriceWithoutCurrency, 10);

                //Find Total price
                this.product.getFoundProduct(selectedItem)
                    .parent('td.product-name')
                    .siblings('td.product-subtotal')
                    .find('.woocommerce-Price-amount.amount')
                    .invoke('text')
                    .then((totalPriceText) => {
                        //Text conversion to number
                        const totalPriceWithoutCurrency = totalPriceText.replace(/[^0-9]/g, '');
                        const totalPrice = parseInt(totalPriceWithoutCurrency, 10);

                        //Count Total price
                        const expectedTotalPrice = unitPrice * quantity;

                        //Compare total price displayed to counted 
                        expect(totalPrice).to.equal(expectedTotalPrice);
                    });
            });
    }
    
    getCartPageUrl() {
        return cy.url().should('include', cartUrl);
    }
}