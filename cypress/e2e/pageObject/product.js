export const item = {
    firstItem: "manago shirt",
    secondItem: "andora scarf",
    thirdItem: "blue magawi shoes"
}
export const addToCartButton = ".single_add_to_cart_button";
export const quantityInputField = ".input-text";
export const productPageUrl = "https://skleptest.pl/product/";

export default class Product {
    getFoundProduct(selectedItem) {
        //Converting first word letter from lower case to upper case
        const convertedItem = selectedItem.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
        return cy.contains(convertedItem);
    }

    getQuantityInputField() {
        return cy.get(quantityInputField);
    }

    getAddToCartButton() {
        return cy.get(addToCartButton);
    }

    getProductPageUrl(selectedItem) {
        //Changing " " to "-"
        const productName = selectedItem.replace(/ /g, '-');
        return cy.url().should('include', productPageUrl + productName + "/");
    }
}