export const registerationEmailInputField = '#reg_email';
export const registerationPasswordInputField = '#reg_password';
export const registerButton = '.woocommerce-Button.button[name="register"]';
export const accountUrl = 'https://skleptest.pl/my-account/';
export const validationMessage = {
    Location: '.woocommerce-error',
    EmptyEmailAndPasswordValidationText: 'Please provide a valid email address.',
    EmptyPasswordValidationText: 'Error: Please enter an account password.',
    EmptyEmailValidationText: 'Error: Please provide a valid email address.'
}
export const passwordStrenght = {
    Location: '.woocommerce-password-strength',
    VeryWeakPassword: 'Very weak - Please enter a stronger password.',
    WeakPassword: 'Weak - Please enter a stronger password.',
    MediumPassword: 'Medium',
    StrongPassword: 'Strong'
}
export const generatedEmail = {
    UserName: `guild.qa.testing+${Date.now()}`,
    Domain: '@gmail.com'
}
export const userNameAfterLogin = generatedEmail.UserName.replace("+", "");
export const password = {
    StrongPassword: `GuildQATeam1!+${Date.now()}`,
    VeryWeakPassword: 'Gui',
    WeakPassword: 'GuildQAT',
    MediumPassword: 'GuildQATeam'
}

export default class Register {

    enterRegistrationEmailInputField() {
        return cy.get(registerationEmailInputField).type(generatedEmail.UserName + generatedEmail.Domain);
    }

    getRegistrationEmailInputField() {
        return cy.get(registerationEmailInputField);
    }

    enterRegisterationPasswordInputField() {
        return cy.get(registerationPasswordInputField).type(password.StrongPassword);
    }

    getRegisterationPasswordInputField() {
        return cy.get(registerationPasswordInputField);
    }

    getRegisterButton() {
        return cy.get(registerButton);
    }

    waitForAccountUrlToBeLoaded() {
        return cy.waitUntil(() => {
            return cy.url().should('eq', accountUrl);
        });
    }

    getValidationMessageNoEmailAndPassword() {
        return cy.get(validationMessage.Location)
            .should('be.visible')
            .contains(validationMessage.EmptyEmailAndPasswordValidationText);
    }

    getValidationMessageNoEmail() {
        return cy.get(validationMessage.Location)
            .should('be.visible')
            .contains(validationMessage.EmptyEmailValidationText);
    }

    getValidationMessageNoPassword() {
        return cy.get(validationMessage.Location)
            .should('be.visible')
            .contains(validationMessage.EmptyPasswordValidationText);
    }

    checkPasswordStrenghVeryWeak() {
        return cy.get(passwordStrenght.Location)
            .should('be.visible')
            .contains(passwordStrenght.VeryWeakPassword);
    }

    checkPasswordStrenghWeak() {
        return cy.get(passwordStrenght.Location)
            .should('be.visible')
            .contains(passwordStrenght.WeakPassword);
    }

    checkPasswordStrenghMedium() {
        return cy.get(passwordStrenght.Location)
            .should('be.visible')
            .contains(passwordStrenght.MediumPassword);
    }
    checkPasswordStrenghStrond() {
        return cy.get(passwordStrenght.Location)
            .should('be.visible')
            .contains(passwordStrenght.StrongPassword);
    }
}