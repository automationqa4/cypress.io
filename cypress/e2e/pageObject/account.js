export default class Account{
    getLogOutButtonFromMenu(){
        cy.get("li").parents(".woocommerce-MyAccount-navigation").find("li").contains("Logout").click();
    }
}