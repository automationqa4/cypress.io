const request = require('supertest');

describe('Random Dog Image', () => {
  it('Get random doggo', async () =>{
    await request('https://dog.ceo') 
      .get('/api/breeds/image/random')
      .expect(200)
  });
});